-- MySQL dump 10.13  Distrib 5.5.24, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: mydatabase
-- ------------------------------------------------------
-- Server version	5.5.24-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_1e014c8f` (`permission_id`),
  KEY `auth_group_permissions_425ae3c4` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_message`
--

DROP TABLE IF EXISTS `auth_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_message` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `auth_message_403f60f` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_message`
--

LOCK TABLES `auth_message` WRITE;
/*!40000 ALTER TABLE `auth_message` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_1bb8f392` (`content_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add permission',1,'add_permission'),(2,'Can change permission',1,'change_permission'),(3,'Can delete permission',1,'delete_permission'),(4,'Can add group',2,'add_group'),(5,'Can change group',2,'change_group'),(6,'Can delete group',2,'delete_group'),(7,'Can add user',3,'add_user'),(8,'Can change user',3,'change_user'),(9,'Can delete user',3,'delete_user'),(13,'Can add content type',5,'add_contenttype'),(14,'Can change content type',5,'change_contenttype'),(15,'Can delete content type',5,'delete_contenttype'),(16,'Can add session',6,'add_session'),(17,'Can change session',6,'change_session'),(18,'Can delete session',6,'delete_session'),(19,'Can add site',7,'add_site'),(20,'Can change site',7,'change_site'),(21,'Can delete site',7,'delete_site'),(22,'Can add log entry',8,'add_logentry'),(23,'Can change log entry',8,'change_logentry'),(24,'Can delete log entry',8,'delete_logentry'),(25,'Can add event',9,'add_event'),(26,'Can change event',9,'change_event'),(27,'Can delete event',9,'delete_event'),(28,'Can add advertisingmethod',10,'add_advertisingmethod'),(29,'Can change advertisingmethod',10,'change_advertisingmethod'),(30,'Can delete advertisingmethod',10,'delete_advertisingmethod'),(31,'Can add member',11,'add_member'),(32,'Can change member',11,'change_member'),(33,'Can delete member',11,'delete_member'),(34,'Can add record',12,'add_record'),(35,'Can change record',12,'change_record'),(36,'Can delete record',12,'delete_record'),(37,'Can add csv import',13,'add_csvimport'),(38,'Can change csv import',13,'change_csvimport'),(39,'Can delete csv import',13,'delete_csvimport'),(40,'Can add import model',14,'add_importmodel'),(41,'Can change import model',14,'change_importmodel'),(42,'Can delete import model',14,'delete_importmodel'),(43,'Can add professional event',15,'add_professionalevent'),(44,'Can add professional record',16,'add_professionalrecord'),(45,'Can change professional event',15,'change_professionalevent'),(46,'Can change professional record',16,'change_professionalrecord'),(47,'Can delete professional event',15,'delete_professionalevent'),(48,'Can delete professional record',16,'delete_professionalrecord');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `password` varchar(128) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `last_login` datetime NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'anthony','','','a@a.com','pbkdf2_sha256$10000$8wJH7ag2croM$X492JVGWNGHohjssOCEh7dec3n44FpCFSWjQwl2TZns=',1,1,1,'2012-09-23 20:48:11','2012-02-10 03:39:00');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_403f60f` (`user_id`),
  KEY `auth_user_groups_425ae3c4` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_1e014c8f` (`permission_id`),
  KEY `auth_user_user_permissions_403f60f` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `csvimport_csvimport`
--

DROP TABLE IF EXISTS `csvimport_csvimport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `csvimport_csvimport` (
  `id` int(11) NOT NULL,
  `model_name` varchar(255) NOT NULL,
  `field_list` varchar(255) NOT NULL,
  `upload_file` varchar(100) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `encoding` varchar(32) NOT NULL,
  `upload_method` varchar(50) NOT NULL,
  `error_log` text NOT NULL,
  `import_date` date NOT NULL,
  `import_user` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `csvimport_csvimport`
--

LOCK TABLES `csvimport_csvimport` WRITE;
/*!40000 ALTER TABLE `csvimport_csvimport` DISABLE KEYS */;
/*!40000 ALTER TABLE `csvimport_csvimport` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `csvimport_importmodel`
--

DROP TABLE IF EXISTS `csvimport_importmodel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `csvimport_importmodel` (
  `id` int(11) NOT NULL,
  `csvimport_id` int(11) NOT NULL,
  `numeric_id` int(10) unsigned NOT NULL,
  `natural_key` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `csvimport_importmodel_1f71b7a` (`csvimport_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `csvimport_importmodel`
--

LOCK TABLES `csvimport_importmodel` WRITE;
/*!40000 ALTER TABLE `csvimport_importmodel` DISABLE KEYS */;
/*!40000 ALTER TABLE `csvimport_importmodel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `object_id` text,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_1bb8f392` (`content_type_id`),
  KEY `django_admin_log_403f60f` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2012-02-10 03:42:03',1,11,'234','1',1,''),(2,'2012-02-10 03:42:40',1,11,'234','a2',2,'Changed pid.'),(3,'2012-02-10 03:46:51',1,9,'1','2012-02-10',1,''),(4,'2012-02-10 04:03:32',1,11,'234','a2',3,''),(5,'2012-02-10 22:52:29',1,11,'191','sosso',2,'Changed idnum, pid, email, advertisingmethod, referername, event1_22_11, event9_10_11, event10_27_11, event2_2_2012, eventSocial2_4_2012 and event2_9_2012.'),(6,'2012-02-10 22:52:43',1,9,'2','2012-02-10',1,''),(7,'2012-02-10 23:10:50',1,9,'1','2012-02-10',3,''),(8,'2012-02-10 23:21:38',1,11,'234','test',1,''),(9,'2012-02-15 02:07:52',1,9,'19','2012-02-15',1,''),(10,'2012-02-15 02:21:44',1,11,'234','test',2,'Changed idnum and event1_22_11.'),(11,'2012-02-15 20:06:04',1,11,'235','karinw',2,'Changed idnum, pid, email, studenttype, membershipexpiration, referername, paymentinfo, event1_22_11, event9_8_11, event9_10_11, event9_15_11, event9_29_11, event10_6_11, event10_13_11, eventsocial10_15, event10_20_11, event10_27_11, event11_3_11, event11_10_11, event12_1_11, event1_19_2012, event1_26_2012, event2_2_2012, eventSocial2_4_2012 and event2_9_2012.'),(12,'2012-02-16 17:00:49',1,9,'20','2012-02-16',1,''),(13,'2012-02-16 17:39:29',1,11,'234','test',2,'Changed idnum.'),(14,'2012-02-16 21:22:58',1,11,'169','luke6a52@vt.edu',2,'Changed pid, lastname, studenttype, advertisingmethod, referername, event1_22_11, event9_10_11, event9_15_11, event10_6_11 and eventsocial10_15.'),(15,'2012-02-16 21:30:49',1,11,'191','sosso',2,'Changed idnum and event1_22_11.'),(16,'2012-02-16 21:51:05',1,11,'235','karinw',3,''),(17,'2012-02-20 01:27:51',1,11,'86','',3,''),(18,'2012-02-22 18:32:18',1,9,'21','2012-02-23',1,''),(19,'2012-02-22 18:39:48',1,9,'22','2012-02-22',1,''),(20,'2012-02-23 06:33:51',1,10,'18','Karin Wiles',2,'Changed name.'),(21,'2012-02-23 10:02:27',1,9,'21','2012-02-23',3,''),(22,'2012-02-23 10:03:48',1,9,'23','2012-02-23',1,''),(23,'2012-02-23 10:04:45',1,12,'711','Mike Cantrell 2012-02-23',3,''),(24,'2012-02-24 12:25:31',1,9,'24','2012-02-25',1,''),(25,'2012-03-15 19:35:03',1,9,'25','2012-03-15',1,''),(26,'2012-09-10 21:18:09',1,9,'26','2012-09-10',1,''),(27,'2012-09-11 01:19:01',1,9,'27','2012-09-11',1,''),(28,'2012-09-11 13:19:49',1,12,'785','Anthony Sosso 2012-09-11',3,''),(29,'2012-09-11 13:21:35',1,12,'784','Anthony Sosso 2012-09-11',3,''),(30,'2012-09-11 13:21:35',1,12,'783','Anthony Sosso 2012-09-11',3,''),(31,'2012-09-11 13:21:35',1,12,'782','Anthony Sosso 2012-09-11',3,''),(32,'2012-09-11 13:21:35',1,12,'781','Anthony Sosso 2012-09-11',3,''),(33,'2012-09-11 13:21:35',1,12,'780','Anthony Sosso 2012-09-11',3,''),(34,'2012-09-11 13:21:35',1,12,'779','Anthony Sosso 2012-09-11',3,''),(35,'2012-09-11 13:21:35',1,12,'778','Anthony Sosso 2012-09-11',3,''),(36,'2012-09-11 13:21:35',1,12,'777','Anthony Sosso 2012-09-11',3,''),(37,'2012-09-11 13:21:35',1,12,'776','Anthony Sosso 2012-09-11',3,''),(38,'2012-09-11 13:21:35',1,12,'775','Anthony Sosso 2012-09-11',3,''),(39,'2012-09-11 13:22:14',1,12,'773','Anthony Sosso 2012-09-10',3,''),(40,'2012-09-11 13:22:14',1,12,'772','Anthony Sosso 2012-09-10',3,''),(41,'2012-09-11 13:22:14',1,12,'771','Anthony Sosso 2012-09-10',3,''),(42,'2012-09-11 13:32:06',1,9,'27','2012-09-11',1,''),(43,'2012-09-11 13:33:50',1,11,'284','sosso',3,''),(44,'2012-09-11 13:33:50',1,11,'283','sosso',3,''),(45,'2012-09-11 13:33:50',1,11,'282','sosso',3,''),(46,'2012-09-11 13:33:50',1,11,'281','sosso',3,''),(47,'2012-09-11 13:33:50',1,11,'280','sosso',3,''),(48,'2012-09-11 13:33:50',1,11,'279','sosso',3,''),(49,'2012-09-11 13:33:50',1,11,'278','sosso',3,''),(50,'2012-09-11 13:33:50',1,11,'277','sosso',3,''),(51,'2012-09-11 13:33:50',1,11,'276','sosso',3,''),(52,'2012-09-11 13:33:50',1,11,'275','sosso',3,''),(53,'2012-09-11 13:33:50',1,11,'274','sosso',3,''),(54,'2012-09-11 13:33:50',1,11,'273','sosso',3,''),(55,'2012-09-11 13:33:50',1,11,'272','sosso',3,''),(56,'2012-09-11 13:33:50',1,11,'271','sosso',3,''),(57,'2012-09-11 13:33:50',1,11,'270','sosso',3,''),(58,'2012-09-11 13:33:50',1,11,'269','sosso',3,''),(59,'2012-09-11 13:33:50',1,11,'268','sosso',3,''),(60,'2012-09-11 13:33:50',1,11,'267','sosso',3,''),(61,'2012-09-11 13:33:50',1,11,'266','sosso',3,''),(62,'2012-09-11 13:34:00',1,11,'247','test13',3,''),(63,'2012-09-11 13:45:50',1,12,'772','Stephanie Hippert 2012-09-11',3,''),(64,'2012-09-11 13:45:50',1,12,'771','George Buhl 2012-09-11',3,''),(65,'2012-09-18 11:43:30',1,9,'28','2012-09-18',1,''),(66,'2012-09-18 13:58:45',1,15,'1','2012-09-18',1,'');
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_label` (`app_label`,`model`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'permission','auth','permission'),(2,'group','auth','group'),(3,'user','auth','user'),(5,'content type','contenttypes','contenttype'),(6,'session','sessions','session'),(7,'site','sites','site'),(8,'log entry','admin','logentry'),(9,'event','signin','event'),(10,'advertisingmethod','signin','advertisingmethod'),(11,'member','signin','member'),(12,'record','signin','record'),(13,'csv import','csvimport','csvimport'),(14,'import model','csvimport','importmodel'),(15,'professional event','signin','professionalevent'),(16,'professional record','signin','professionalrecord');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` text NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_3da3d3d8` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('0b7642b3c860a293bd38da688f7f69d3','YWM0NGZjM2U0ZDdjNTU0MDE2OGQ1OGRhYTc4YWU5NmIwZGRiMTc0ODqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLAXUu\n','2012-10-02 17:38:17'),('270eb6657204e154fe67585079ea5792','YjUwMjkxZmFmNjllY2QxOTI0Nzc3ZDFhODk1ZjBhOTAwZWZkN2U4ZTqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQF1Lg==\n','2012-10-07 20:48:11'),('3a433cf5209839aa5b182296a93e2c84','gAJ9cQFVCnRlc3Rjb29raWVxAlUGd29ya2VkcQNzLmZhN2RmOWQ0NmZkM2QyNWNmOWViZTE3OTA2\nNWM1MWYw\n','2012-03-08 06:35:40'),('3ad8a3376893731abd7fc338e8d32322','gAJ9cQEoVRJfYXV0aF91c2VyX2JhY2tlbmRxAlUpZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5k\ncy5Nb2RlbEJhY2tlbmRxA1UNX2F1dGhfdXNlcl9pZHEESwF1LjI0ZDlhYzRmYmU5ZTBhMGFmN2Y5\nMzNjMDhjMGUzNGQ5\n','2012-03-08 18:11:01'),('405087b81d88efc2d9e8c56d5c609dac','gAJ9cQEoVRJfYXV0aF91c2VyX2JhY2tlbmRxAlUpZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5k\ncy5Nb2RlbEJhY2tlbmRxA1UNX2F1dGhfdXNlcl9pZHEESwF1LjI0ZDlhYzRmYmU5ZTBhMGFmN2Y5\nMzNjMDhjMGUzNGQ5\n','2012-03-08 10:01:03'),('44f819bd378290b5474e3fbfba65d784','YWM0NGZjM2U0ZDdjNTU0MDE2OGQ1OGRhYTc4YWU5NmIwZGRiMTc0ODqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLAXUu\n','2012-02-24 03:40:45'),('541620bda55afd5aec431aec48454c1c','gAJ9cQEoVRJfYXV0aF91c2VyX2JhY2tlbmRxAlUpZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5k\ncy5Nb2RlbEJhY2tlbmRxA1UNX2F1dGhfdXNlcl9pZHEESwF1LjI0ZDlhYzRmYmU5ZTBhMGFmN2Y5\nMzNjMDhjMGUzNGQ5\n','2012-03-08 18:07:57'),('6bf53e2f1978346403e8cad19bdf60cf','gAJ9cQEoVRJfYXV0aF91c2VyX2JhY2tlbmRxAlUpZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5k\ncy5Nb2RlbEJhY2tlbmRxA1UNX2F1dGhfdXNlcl9pZHEESwF1LjI0ZDlhYzRmYmU5ZTBhMGFmN2Y5\nMzNjMDhjMGUzNGQ5\n','2012-03-01 19:34:25'),('82c7502113248d18b633c45cb95df960','gAJ9cQEoVRJfYXV0aF91c2VyX2JhY2tlbmRxAlUpZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5k\ncy5Nb2RlbEJhY2tlbmRxA1UNX2F1dGhfdXNlcl9pZHEESwF1LjI0ZDlhYzRmYmU5ZTBhMGFmN2Y5\nMzNjMDhjMGUzNGQ5\n','2012-03-29 19:34:57'),('9273df219110b612692a965cc6d51947','YWM0NGZjM2U0ZDdjNTU0MDE2OGQ1OGRhYTc4YWU5NmIwZGRiMTc0ODqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLAXUu\n','2012-10-02 17:41:01'),('a5454474a0ecfbba05ef4da001c6d4f6','YWM0NGZjM2U0ZDdjNTU0MDE2OGQ1OGRhYTc4YWU5NmIwZGRiMTc0ODqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLAXUu\n','2012-10-02 11:43:25'),('b529de280913ce5b5800f70cea659141','gAJ9cQEoVRJfYXV0aF91c2VyX2JhY2tlbmRxAlUpZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5k\ncy5Nb2RlbEJhY2tlbmRxA1UNX2F1dGhfdXNlcl9pZHEESwF1LjI0ZDlhYzRmYmU5ZTBhMGFmN2Y5\nMzNjMDhjMGUzNGQ5\n','2012-03-07 18:31:45'),('eeed69351b0c78af870c0b91b0c8933f','YWM0NGZjM2U0ZDdjNTU0MDE2OGQ1OGRhYTc4YWU5NmIwZGRiMTc0ODqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLAXUu\n','2012-10-02 17:06:01'),('f5d39bb693838adf47734eaabd79dd9b','gAJ9cQFVCnRlc3Rjb29raWVxAlUGd29ya2VkcQNzLmZhN2RmOWQ0NmZkM2QyNWNmOWViZTE3OTA2\nNWM1MWYw\n','2012-03-01 14:02:52'),('f5d5cca74e1981fef1e52f93a64f565f','YWM0NGZjM2U0ZDdjNTU0MDE2OGQ1OGRhYTc4YWU5NmIwZGRiMTc0ODqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQRLAXUu\n','2012-09-24 21:17:57');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_site`
--

DROP TABLE IF EXISTS `django_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_site` (
  `id` int(11) NOT NULL,
  `domain` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_site`
--

LOCK TABLES `django_site` WRITE;
/*!40000 ALTER TABLE `django_site` DISABLE KEYS */;
INSERT INTO `django_site` VALUES (1,'example.com','example.com');
/*!40000 ALTER TABLE `django_site` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `signin_advertisingmethod`
--

DROP TABLE IF EXISTS `signin_advertisingmethod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `signin_advertisingmethod` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `signin_advertisingmethod`
--

LOCK TABLES `signin_advertisingmethod` WRITE;
/*!40000 ALTER TABLE `signin_advertisingmethod` DISABLE KEYS */;
INSERT INTO `signin_advertisingmethod` VALUES (1,'Table Cards (West End)'),(2,'Table Cards (D2)'),(3,'Table Cards (DX)'),(4,'Table Cards (Owens)'),(5,'Table Cards (Hokie Grill)'),(6,'Table Cards (Sbarro)'),(7,'Table Cards (Squires ABP)'),(8,'Table Cards (GLC ABP)'),(9,'A-Frame (West End)'),(10,'A-Frame (D2)'),(11,'A-Frame (Drillfield)'),(12,'A-Frame (McBryde)'),(13,'A-Frame (GBJ)'),(14,'A-Frame (Squires)'),(15,'A-Frame (Library)'),(16,'A-Frame (Owens)'),(17,'test method'),(18,'Karin Wiles'),(19,'Anthony'),(20,'Website'),(21,'bdvtmeeting'),(22,'Kim Schatz'),(23,'Jen Gorce'),(24,'Friend'),(25,'Gobblerfest 2012'),(26,'Gobblerfest 2013');
/*!40000 ALTER TABLE `signin_advertisingmethod` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `signin_event`
--

DROP TABLE IF EXISTS `signin_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `signin_event` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `signin_event`
--

LOCK TABLES `signin_event` WRITE;
/*!40000 ALTER TABLE `signin_event` DISABLE KEYS */;
INSERT INTO `signin_event` VALUES (1,'2011-01-22'),(2,'2011-09-08'),(3,'2011-09-10'),(4,'2011-09-15'),(5,'2011-09-29'),(6,'2011-10-06'),(7,'2011-10-13'),(8,'2011-10-15'),(9,'2011-10-20'),(10,'2011-10-27'),(11,'2011-11-03'),(12,'2011-11-10'),(13,'2011-12-01'),(14,'2012-01-19'),(15,'2012-01-26'),(16,'2012-02-02'),(17,'2012-02-04'),(18,'2012-02-09'),(19,'2012-02-15'),(20,'2012-02-16'),(22,'2012-02-22'),(23,'2012-02-23'),(24,'2012-02-25'),(25,'2012-03-15'),(26,'2012-09-10'),(27,'2012-09-11'),(28,'2012-09-18');
/*!40000 ALTER TABLE `signin_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `signin_member`
--

DROP TABLE IF EXISTS `signin_member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `signin_member` (
  `membership_expiration_date` datetime DEFAULT NULL,
  `first_event_date` datetime DEFAULT NULL,
  `id` int(11) NOT NULL,
  `idnum` varchar(20) DEFAULT NULL,
  `pid` varchar(20) DEFAULT NULL,
  `firstname` varchar(20) DEFAULT NULL,
  `lastname` varchar(20) DEFAULT NULL,
  `email` varchar(20) DEFAULT NULL,
  `studenttype` varchar(20) DEFAULT NULL,
  `membershipexpiration` varchar(20) DEFAULT NULL,
  `advertisingmethod` varchar(20) DEFAULT NULL,
  `referername` varchar(20) DEFAULT NULL,
  `paymentinfo` varchar(20) DEFAULT NULL,
  `event9_8_11` varchar(20) DEFAULT NULL,
  `event9_10_11` varchar(20) DEFAULT NULL,
  `event9_15_11` varchar(20) DEFAULT NULL,
  `event9_29_11` varchar(20) DEFAULT NULL,
  `event10_6_11` varchar(20) DEFAULT NULL,
  `event10_13_11` varchar(20) DEFAULT NULL,
  `eventsocial10_15` varchar(20) DEFAULT NULL,
  `event10_20_11` varchar(20) DEFAULT NULL,
  `event10_27_11` varchar(20) DEFAULT NULL,
  `event11_3_11` varchar(20) DEFAULT NULL,
  `event11_10_11` varchar(20) DEFAULT NULL,
  `event12_1_11` varchar(20) DEFAULT NULL,
  `event1_19_2012` varchar(20) DEFAULT NULL,
  `event1_26_2012` varchar(20) DEFAULT NULL,
  `event2_2_2012` varchar(20) DEFAULT NULL,
  `eventSocial2_4_2012` varchar(20) DEFAULT NULL,
  `event2_9_2012` varchar(20) DEFAULT NULL,
  `event1_22_11` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `signin_member`
--

LOCK TABLES `signin_member` WRITE;
/*!40000 ALTER TABLE `signin_member` DISABLE KEYS */;
INSERT INTO `signin_member` VALUES ('2013-03-17 00:00:00',NULL,1,'','','Katheryn','Abruzzo','kra0828@vt.edu','U','','Anthony','','','','','','','p','','','','','','','','','','','','',NULL),(NULL,NULL,2,'905608204','','noname','Al-Mamun','samamun@vt.edu','g','12-spring','From Friends','','25','P','','p','p','p','p','','','p','','','p','p','p','p','','p',NULL),(NULL,NULL,3,'905400080','','Jeremy','Almeter','','U','12-Spring','','','25','p','p','p','','','p','','p','p','','','p','p','p','','p','p',NULL),(NULL,NULL,4,'','','Nick','Ascosi','ascosi8@vt.edu','U','','Dining Card','','','','','','','p','','','','','','','','','','','','',NULL),(NULL,NULL,5,'','','Sima','Azarani','sima4@vt.edu','','12-Spring','','','25','p','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,6,'','','Puranjoy','Bhattacharjee','puran@vt.edu','G','11 fall','','','15','p','x','','P','p','p','','p','p','p','','p','','','','','',NULL),(NULL,NULL,7,'','','Matthew','Blower','test','','','','','','','','','','','','','','','','','','','p','','','',NULL),(NULL,NULL,8,'','','Bryan','Blue','','U','','Kelsy Trimble','','','','','','','p','','','','','','','','','','','','',NULL),(NULL,NULL,9,'','','Jenna','Boyer','jmb9tb@virginia.edu','','','','','','','','','','','','','','p','','','','','','','','',NULL),(NULL,NULL,10,'','','Racis','Breeze','','','','','','','p','p','','','','','','','','','','','','p','','','',NULL),(NULL,NULL,11,'','','Joanna','Brindise','brindise@vt.edu','U','','','','','','','','','p','','','','','','','','','','','','',NULL),(NULL,NULL,12,'','','Cameron','Brooks','camero59@vt.edu','','','','','','','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,13,'','','Pat','Brousseau','','','','','','','p','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,14,'','','Rachel','Brown','','','','','','','p','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,15,'','','Lorenza','Brunfield','','','','','','','','','','','','','','p','','','','','','','','','',NULL),(NULL,NULL,16,'','','Melissa','Brunfield','','','','','','','','','','','','','','p','','','','','','','','','',NULL),(NULL,NULL,17,'','','Adrienne','Brunger','abrunger@vt.edu','','','','','','p','','p','','','','','','','','','','','','','','',NULL),(NULL,NULL,18,'','','Marina','Burkova','','','','','','','','x','p','','','','','','','','','','','','','','',NULL),(NULL,NULL,19,'','','John','Butler','','','','','','','','','','','','','','','','','','','','','','','',NULL),('2013-09-13 00:00:00',NULL,20,'905508985','','Beth','Cameron','','u','','','','','','x','','P','p','','','','p','','','','p','P','p','','',NULL),(NULL,NULL,21,'','','Keith','Canales','','','','','','','p','','p','','','','','','','','','','','','','','',NULL),(NULL,NULL,22,'905287726','','Mike','Cantrell','','u','','','','','p','p','p','p','p','p','','p','p','p','p','p','p','p','','','p',NULL),(NULL,NULL,23,'','','Lindsay','Carr','','','','','','','p','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,24,'','','Eleonor','Cayford','ecayford@yahoo.com','','','Jen Groce','','','','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,25,'','','Eleonor','Cayford','eleonorc@vt.edu','g','','','','','','','','','','','','p','','p','p','','','','','','',NULL),(NULL,NULL,26,'','','Brian','Chang','brianc91@vt.edu','u','','','','','','x','','','','','','','','','','','','','','p','',NULL),(NULL,NULL,27,'','','Gloria','Cheng','gchneg08@vt.edu','','','','','','','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,28,'','','Jose','Claros','marce623@vt.edu','U','','','','','','','','','','','','','','','','','','','','p','',NULL),(NULL,NULL,29,'905561882','','Kristen','Clermont','','','','','','','','','','','','','','','','','','','','','p','','',NULL),(NULL,NULL,30,'','','Lisa','Cooper','','','','','','','','','','p','','','','','','','','','','','','','',NULL),(NULL,NULL,31,'','','Scotty','Coye','','U','12-Spring','','','25','p','','p','p','p','p','','','p','','','','','','','','',NULL),(NULL,NULL,32,'','','Charlotte','Crump','chxthump@vt.edu','u','','kim','','','','','','','','','','','p','','','','','','','','',NULL),(NULL,NULL,33,'','','Jamie','Dalrymple','jamied93@vt.edu','','','Cat','','','','','','','','','','','','','','','p','','','','',NULL),(NULL,NULL,34,'','','Aaron','Dalton','','','','Katie Sherman','','','','','','','','','','','','','','','p','','','','',NULL),(NULL,NULL,35,'','','Kathryn','Doebel','kitkat6@vt.edu','','','','','','','','','','','','','','','','','','','p','','','',NULL),(NULL,NULL,36,'','','Kelly','Drews','kcdrews@vt.edu','u','','','','','','','','','','p','','','p','','','','','','','','',NULL),(NULL,NULL,37,'905375303','','Aja','Eddins','aevt@vt.edu','','spring 12','','','','p','','p','','','p','','','p','p','p','','','','','','',NULL),(NULL,NULL,38,'905466821','','Ruthie','Edwards','','','','','','','','','','','','','','','','','','','p','','p','','',NULL),(NULL,NULL,39,'','','Wesley','Edye','','','','','','','p','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,40,'','','','Elizabeth Mack','','','','','','','','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,41,'','','Derek','Ellison','','','','','','','p','','p','','','','','','','','','','','','','','',NULL),(NULL,NULL,42,'','','Allen','Ernst','','U','','Karin Wiles and Kati','','','','','','','p','','','','','','','','','','','','',NULL),(NULL,NULL,43,'905479781','','Bryan','Evans','bryan.evans@vt.edu','u','','Katie Sherman','','','','x','','','','','','','','','','','','','','p','',NULL),(NULL,NULL,44,'','','Taylor','Everson','taylor10@vt.edu','','','','','','p','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,45,'','','Jennifer','Fairing','jennsf24@vt.edu','','','','','','','','','p','','p','','','','','','','','','','','',NULL),(NULL,NULL,46,'','','Derric','Featherstone','','','','','','','p','','','w','','','','','','','','','','','','','',NULL),(NULL,NULL,47,'','','noname','Felix','fyq@vt.edu','G','','FB','','fall 11','','x','','','p','p','','','','','','','','p','','','',NULL),(NULL,NULL,48,'','','Scott','Fernandez','','','12-Spring','','','25','p','','','','p','','','','','','','','','','','','',NULL),(NULL,NULL,49,'','','Kyle','Ferris','','','','','','','','','','','','','','p','','','','','','','','','',NULL),(NULL,NULL,50,'905615846','','Brenna*','Fitzpatrick','','','','','','','p','','p','','','p','','','','p','p','p','p','p','p','','',NULL),(NULL,NULL,51,'','','Trish','Foley','','','','','','','p','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,52,'','','Stephanie','Folkerts','sef1225@vt.edu','U','','','','spring 12','p','','p','p','p','p','','','p','','','','','','','','',NULL),(NULL,NULL,53,'','','Adrian','Forster','forstera@vt.edu','','','','','','p','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,54,'','','Ben','Fox','','','','','','','p','','','','','p','','','','','','','','','','','',NULL),(NULL,NULL,55,'','','Billy','Freer','','','','','','','p','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,56,'','','Thomas','Friss','thomasf2@vt.edu','','','','','','p','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,57,'905342644','','Connie','Fu','conni3fu@vt.edu','','','','','','','','','','','','','','','','','','','','p','','',NULL),(NULL,NULL,58,'','','Becca','Fudtle','','','','','','','p','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,59,'','','Abby','Garrett','abbyg06@vt.edu','','12-Spring','','','25','','','','p','','p','','','p','','','','','','','','',NULL),(NULL,NULL,60,'','','Erina','Gateley',NULL,'','','','','','p','p','p','','','','','p','','','','','','','','','',NULL),(NULL,NULL,61,'','','Abby','Gatet','','','','','','','','x','p','','','','','','','','','','','','','','',NULL),(NULL,NULL,62,'','','Kelson','Gent','kelsong@vt.edu','','','','','','','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,63,'','','Andrew','George','abg1331@vt.edu','','','knew emily','','','','','','','','','','','','','p','','','','','','',NULL),(NULL,NULL,64,'','','Jacob','George','','','12-Spring','','','25','p','p','p','p','p','p','','','','','','','','','','','',NULL),(NULL,NULL,65,'','','Stacey','Gerben','srgerb@vt.edu','','','','','','','','','','','','','p','p','','','','p','p','','','',NULL),(NULL,NULL,66,'','','Nick','Gigliotti','nickg93@vt.edu','','','','','','','','','','','p','','','','','','','','','','','',NULL),(NULL,NULL,67,'','','Corey','Gilbert','','','12 spring','','','25','','','','','','','','p','p','p','','','','','','','',NULL),(NULL,NULL,68,'','','Liz','Gladwin','egladwin@nanosonic.c','','','','','','','','','','','','','','','','','','','','','','',NULL),('2012-09-12 00:00:00',NULL,69,'905473186','','Lexi','Glagola','leximr@vt.edu','U','12 spring','','','25','p','p','p','p','p','p','','p','p','p','p','p','p','p','','','',NULL),(NULL,NULL,70,'','','Katie','Gloe','kgloe@vt.edu','','','','','','p','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,71,'','','Dan','Goodwin','dangoodwin@vt.edu','u','','tablecard','','','','','','','','','','','p','','','','','','','','',NULL),(NULL,NULL,72,'905533942','','Jen','Gorce','jen10@vt.edu','u','12-Spring','','','25','p','','p','p','','p','','p','p','p','p','p','p','','','','',NULL),(NULL,NULL,73,'905417735','','Zachary','Greer','','','12- spring','','','25','p','','p','','','','','p','p','p','','','p','P','p','p','p',NULL),(NULL,NULL,74,'','','Catherine','Grey','','','','','','','p','','','','','','','','','','','','','P','','','',NULL),('2012-09-12 00:00:00',NULL,75,'905495278','','Laura','Griffin','','','12-Spring','','','25','','','','p','','p','','p','','','p','p','p','p','','','p',NULL),(NULL,NULL,76,'905469643','','Jay','Grimm','','u','12-Spring','','','25','p','p','p','p','p','p','','p','p','p','p','','','','p','','p',NULL),('2013-09-13 00:00:00',NULL,77,'905380540','','Luke','Gusukuma','lukesg08@vt.edu','','12-spring','','','25','p','','p','p','','p','','p','p','p','p','','p','p','p','','p',NULL),(NULL,NULL,78,'','','Michael','Halstead','mth4all@vt.edu','','','','','','','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,79,'','','Behzad','Hamidi','','','','','','','','','','p','','','','','','','','','','','','','',NULL),(NULL,NULL,80,'','','Elmira','Hamidi','hamidi.elmira@gmail.','','','','','','','','','p','','','','','','','','','','','','','',NULL),(NULL,NULL,81,'480335484','','Rachel','Hansen','','','','','','','','','','','','','','','','','','','','','','p','',NULL),(NULL,NULL,82,'905599121','','Talmage','Hansen','','','','','','','','','','','','','','','','','','','','','','p','',NULL),(NULL,NULL,83,'','','Laura','Harris','','','','','','','p','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,84,'','','Mohamed','Hassan','','','','','','','p','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,85,'','','Kathryn','Hastings','','','','','','','p','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,87,'','','Elizabeth','Hiesler','','U','','anthony','','','','','','','p','','','','','','','','','','','','',NULL),(NULL,NULL,88,'','','Bryan','Higgs','','','','Karin Wiles','','','','','','','','','','','','','','','p','','','p','',NULL),(NULL,NULL,89,'','','Bre','Hoisingten','','','','','','','','','','','','','','','','','','','','','','','',NULL),('2012-09-12 00:00:00',NULL,90,'905603478','','Sasha','Howes','sasha92@vt.edu','u','12-2pring','','','25','p','','p','p','','p','','p','p','','','p','','','p','','p',NULL),(NULL,NULL,91,'','','Zach','Hricz','zphricz@vt.edu','U','','Karin Wiles','','','','','','','p','','','','','','','','','','','','',NULL),(NULL,NULL,92,'','','Wesley','Hurd','','','','','','','','','','p','','','','','','','','','','','','','',NULL),(NULL,NULL,93,'','','Jon','Hutchins','','','','Karin Wiles :)','','','','','','','','','','','','','','','','p','','','',NULL),(NULL,NULL,94,'','','Tori','Hyde','','U','','','','','','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,95,'','','Tori','Hyde','','','swing','','','','','','','','','','','','','p','','','','','','','',NULL),(NULL,NULL,96,'','','Sarah','Iler','ski22@vt.edu','','','','','','p','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,97,'','','Samantha','Jeffreys','samj91@vt.edu','','','','','','','','','p','','','','','','','','','','','','','',NULL),(NULL,NULL,98,'','','Eric','Johnson','','','','','','','p','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,99,'','','Sarah','Johnson','','','','','','','','','','','','','','','','','','','','p','','','',NULL),(NULL,NULL,100,'','','Mathew','Jones','','','','','','','','','','','','','','p','','','','','','','','','',NULL),(NULL,NULL,101,'','','Karuna','Joshi','','','','','','','','','','p','','','','','','','','','','','','','',NULL),(NULL,NULL,102,'','','Kelsey','Joston','','','','','','','p','','p','','','','','','','','','','','','','','',NULL),(NULL,NULL,103,'','','Matthew','Kennedy','mkenned2@vt.edu','','','tablecard','','','','','','','','','','','p','p','','p','','','','','',NULL),('2012-09-19 00:00:00',NULL,104,'905513263','','Micah','Kennedy','micah6@vt.edu','','','tablecard','','','','','','','','','','','p','','','','p','p','p','','p',NULL),(NULL,NULL,105,'','','Trevor','Kennedy','','g','','','','','','','','p','','','','','','','','','','','','','',NULL),(NULL,NULL,106,'','','Yasamin','Khorramzadeh','yasi@vt.edu','','','','','','','','','','','','','','','','','','','p','','','',NULL),(NULL,NULL,107,'','','James','Kim.','','','','','','','p','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,108,'','','Diane','Kjelby','dkjelby3@vt.edu','','','','','','','p','','','','','','','','','','','','','','','',NULL),(NULL,NULL,109,'','','Lindsey','Konchuba','lindmk4@vt.edu','U','','','','','','','','','p','','','','','','','','','','','','',NULL),(NULL,NULL,110,'','','Brandon','Kulback','','','swing','','','','','','','p','','','','','p','','','','','p','','','',NULL),(NULL,NULL,111,'','','Lindsey','Kummer','','','','','','','','','','','','','','','','','','','p','','','','',NULL),(NULL,NULL,112,'','','Paul','Kurlak','paul@kurlak.com','u','','Katie Abruzzo','','','','','','','p','','','','','','','','','','','','',NULL),(NULL,NULL,113,'','','Greg','Laplante','grl114@gmail.com','','','Gobblerfest','','','','p','','','','','','','p','','','','','','','','',NULL),(NULL,NULL,114,'','','Myles','Lawlor','','','','been here before','','','','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,115,'','','Ki','Lee','','','','','','','p','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,116,'','','Gao','Lei','','','','','','','p','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,117,'','','Michael','Levet','mlevet@vt.edu','u','','anthony','','','','','','','','p','','','','','','','','','','','',NULL),(NULL,NULL,118,'','','Heshan','Lin','','','','','','','','','','','','','','','','','','','','','p','','',NULL),(NULL,NULL,119,'','','Leah','Linarelli','leah93@vt.edu','','','','','','','','','','','','','p','','','','','','','','','',NULL),(NULL,NULL,120,'','','Jacob','Llaneras','','','','','','','','','','','','','','p','','','','','','P','','','',NULL),(NULL,NULL,121,'','','Sarah','Lyon-Hill','sarahlh@vt.edu','','','','','','','x','p','','','','','','','','','','','','','','',NULL),(NULL,NULL,122,'905491286','','Bri','Lytton','','U','12 spring','','','25','p','x','p','p','p','p','','','p','p','p','p','p','p','p','p','',NULL),(NULL,NULL,123,'905620699','','Sai','Ma','','','','','','','','','','','','','','','','','','','','','p','','',NULL),(NULL,NULL,124,'','','Elizabeth','Mack','emack434@vt.edu','u','Swing','Internet Search','','','','x','','p','p','p','','p','p','p','p','p','','','','','p',NULL),(NULL,NULL,125,'','','Tannistha','Maiti','','','','','','','p','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,126,'','','Kelly','Marlow','','','','','','','','','','','','','','p','','','','','','','','','',NULL),(NULL,NULL,127,'','','Grace','Mawyer','','','','','','','','','','','','','','p','','','','','','','','','',NULL),(NULL,NULL,128,'','','Heather','MccLeivs','','','','','','','p','','p','p','','','','','','','','','','','','','',NULL),(NULL,NULL,129,'','','Sean','McGrady','','u','','kim','','','','','','','','','','','p','p','','','','','','','',NULL),(NULL,NULL,130,'905610272','','Benjamin','McMillan','','','12pring','','','25','p','','','p','','','','','','','','','','P','p','','',NULL),(NULL,NULL,131,'','','Seble','Megussie','','','','','','','p','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,132,'','','Erik','Miller','','','','','','','','','','','','','','','','','','','','','p','','',NULL),(NULL,NULL,133,'','','Kim','MinKyong','','','','','','','','','','','','','','','','','','','','p','','','',NULL),(NULL,NULL,134,'','','Douglas','Minor','','','','','','','','','','','','','','','','','','','p','','','','',NULL),(NULL,NULL,135,'','','Sameer','Miraliev','','','','karin Wiles','','','','','','','','','','','','','','','p','p','','p','',NULL),(NULL,NULL,136,'','','Krystalyn','Monton','kjm2010@vt.edu','','','','','','','','','','','p','','','','','','','','','','','',NULL),(NULL,NULL,137,'','','Pamela','Moore','pmoore34@vt.edu','','','','','','','','','','','','','','p','','','','','','','','',NULL),(NULL,NULL,138,'','','Sade','Moore','sadem93@vt.edu','U','','Dining card','','','','','','P','p','','','','','','','','','','','','',NULL),(NULL,NULL,139,'','','Donal','Murphy','donalm91@vt.edu','','11-fall','','','15','p','','p','','','p','','','','','','','','','','','',NULL),(NULL,NULL,140,'','','Chistopher','Murri','cmurri@vt.edu','','','','','','','','','','','','','p','p','','','','','','','','',NULL),(NULL,NULL,141,'905548627','','Kirby','Myers','known','G','','Ancestral Knowledge','','','','x','','p','','','','p','p','','','','p','','','p','p',NULL),(NULL,NULL,142,'','','Gina','Naranjo','elinazu@hotmail.com','','','','','','','','','','','','','','p','','','','','','','','',NULL),(NULL,NULL,143,'','','Avery','Nelson','','u','','','','','','','','','p','','','','','','','','','','','','',NULL),(NULL,NULL,144,'','','Bailey','Nelson','','','','','','','p','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,145,'','','Peter','Nelson','','','','','','','p','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,146,'','','Duc','Nguyen','','G','12-Spring','','','25','','x','p','p','p','p','','','p','p','','','p','','','','',NULL),(NULL,NULL,147,'','','Peter','Nixon','','','','','','','p','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,148,'905461341','','Dylan','Noffsinger','mdylan@vt.edu','u','','kim','','','','','','','','','','','p','','','','','','','','',NULL),(NULL,NULL,149,'','','Myat','Nyunt','tnmyat2@vt.edu','','','','','','p','','','','','','','','','p','','','p','','p','','',NULL),(NULL,NULL,150,'','','Matt','O\'neil','','U','','Karin Wiles','','','','','','','p','','','','','','','','','','','','',NULL),(NULL,NULL,151,'','','Chris','Olien','','','swing','','','','p','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,152,'','','Andrea','Oliver','andrea9@vt.edu','','','','','','p','p','p','p','','','','','','','','','','','','','',NULL),(NULL,NULL,153,'','','Lera','Oscherov','valeriao@vt.edu','g','','kirby','','','','','','p','','','','','','','','','','','','','',NULL),(NULL,NULL,154,'','','','PaytonVillescas','','','','','','','','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,155,'','','Matthew','Peak','','','','','','','p','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,156,'','','Valerie','Peckarski','vpeck93@vt.edu','','','','','','','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,157,'','','Darrin','Pillman','darrin62@vt.edu','U','12-Spring','','','25','p','','p','p','p','p','','p','','p','p','p','','','','','',NULL),(NULL,NULL,158,'','','Nick','Platt','','','','','','','p','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,159,'','','Douglas','Pudleiner','','','','','','','','','','','','p','','','','','','','','','','','',NULL),(NULL,NULL,160,'','','Breeze','Racis','bracis@vt.edu','','','','','','p','','','P','','','','p','','','p','','','','','','',NULL),(NULL,NULL,161,'','','Chris','Ramos','','','','','','','','','','','','','','','','','','','','p','','','',NULL),(NULL,NULL,162,'','','Pablo','Redden-Gonzalez','pablofrg@vt.edu','','','classes/ internet','','','p','x','','','','','','','','','','','','','','','',NULL),(NULL,NULL,163,'','','James','Reese','','','','','','','p','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,164,'905456747','','Chris','Richards','','u','12 spring','','','15','','','','','','','','','','','','','p','p','p','p','p',NULL),(NULL,NULL,165,'','','Christian','Roa','crroa7@vt.edu','U','','','','','','','','','','','','','','','','','','','','p','',NULL),(NULL,NULL,166,'','','Ginny','Roach','','','','','','','p','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,167,'905434322','','Ethan','Robertson','','U','','','','','','','','','','','','','','','','','p','p','p','','',NULL),('2012-09-19 00:00:00',NULL,168,'905667194','','Mary-Wynn','Rogers','marywynn@vt.edu','u','12 spring','Brit','','25','','','','','','p','','','p','p','p','p','P','P','p','','p',NULL),('2012-09-19 00:00:00',NULL,169,'905542170','luke6a52@vt.edu','Luke','Routhier','luke6a52@vt.edu','u','12 spring','p','p','25','p','p','p','p','p','p','p','p','p','p','p','p','p','p','p','p','p','p'),(NULL,NULL,170,'','','Christina','Rosanl','','','','','','','','','','','','','','','','','','','','P','','','',NULL),(NULL,NULL,171,'','','Robin','Roston','rsroston@vt.edu','','','','','','p','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,172,'905504522','','Travis','Roth','','','','','','','','','','','','','','','','','','','','p','p','','',NULL),(NULL,NULL,173,'905304423','','Thomas','Ruscher','thrusc@vt.edu','','','','','15','','','','','','','','','','','','','','','p','','p',NULL),(NULL,NULL,174,'','','Melissa','Ruth','mruth90@vt.edu','','','friend','','','','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,175,'','','Greg','Ruthton','','','','','','','p','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,176,'','','courtney','Scarborough','court18@vt.edu','','','','','','','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,177,'905418752','','Kim','Schatz','','','','','','','','x','p','','','','','p','p','','','','','p','p','','p',NULL),(NULL,NULL,178,'','','Laurel','Schaefer','laurel91@vt.edu','U','','','','','p','','','p','p','p','','p','p','p','','p','','','','','',NULL),('2013-09-13 00:00:00',NULL,179,'905571673','','Jason','Scherer','','U','12-Spring','','','25','p','','p','p','p','p','','p','p','p','p','p','p','p','','p','p',NULL),(NULL,NULL,180,'','','Luaren','Schoener','laurens1@vt.edu','','','','','','','','','','','p','','','','','','','','','p','','',NULL),(NULL,NULL,181,'','','Kelly','Schofield','','','','','','','','','','','','p','','','','','','','','','','','',NULL),(NULL,NULL,182,'905679239','','Patrick','Seiler','pseiler@vt.edu','G','12-spring','Internet Search','','25','','x','p','p','p','','','p','p','p','p','p','p','','','p','p',NULL),(NULL,NULL,183,'','','Enkhsaruul','Sergelenbaatar','saruul04@vt.edu','','','','','','p','','p','','','','','','','','','','','','','','',NULL),(NULL,NULL,184,'','','Steve','Shaffer','','','','','','','','','','','','','','p','p','','','','','','','','',NULL),(NULL,NULL,185,'','','Waheed','Sheriff','waheed09@vt.edu','','','website','','','','','','','','','','','','','','','','','','','',NULL),('2012-09-12 00:00:00',NULL,186,'905402744','','Katie','Sherman','','u','','','','','p','p','p','p','p','p','p','p','p','p','','','p','p','p','p','',NULL),(NULL,NULL,187,'','','Brittany','Simmons','','','','','','','p','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,188,'','','Josh','Simon','','','','','','','p','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,189,'','','Michael','Simonetti','','','','','','','p','','p','','','','','','','','','','','','','','',NULL),(NULL,NULL,190,'','','Swapna','Sista','swapna27@vt.edu','U','','Karin Wiles and Kati','','','','x','','','','','','','','','','','','p','','','',NULL),('2013-03-17 00:00:00',NULL,191,'905489719','sosso','Anthony','Sosso','sosso@vt.edu','U','11 fall','Google','none','15','p','p','p','p','p','p','p','p','p','p','p','p','p','p','p','p','p','p'),(NULL,NULL,192,'','','Michelle','Sparks','','','','','','','p','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,193,'','','Chelsea','Sponautugle','','','12 spring','Jeremy','','25','','','','','','p','','','','','','','p','','','','',NULL),(NULL,NULL,194,'905535419','','Tod','Spurgen','','','12 spring','','','20','','','','','','p','','','','','p','p','','','','','',NULL),(NULL,NULL,195,'','','Chaky','Sriranganathan','csrirang@vt.edu','','','Karin Wiles','','','','x','','','','','','','','','','','','','','','',NULL),(NULL,NULL,196,'','','Kelsey','Starr','','','','','','','','','','','','p','','p','','p','p','','','','','p','',NULL),(NULL,NULL,197,'905503432','','Loran','Steinberger','bkwrm215@vt.edu','u','','live in hillcrest','','','','','','','','p','','','','','','','','','','','',NULL),(NULL,NULL,198,'','','Porter','Stevens','','','','','','','p','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,199,'','','Jill','Streeter','jill229@vt.edu','','','','','','','','','p','','p','','','','','','','','','','','',NULL),(NULL,NULL,200,'','','Maria','Sugastti','sugmd11@vt.edu','','','','','','p','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,201,'','','noname','Sylvia','','','','','','','p','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,202,'','','Hansen','Talmage','thansen4@vt.edu','Grad','','google search','','','','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,203,'','','Courtney','Tamaro','cat1992@vt.edu','','','','','','p','','','p','','','','','','','','','','','','','',NULL),('2012-09-12 00:00:00',NULL,204,'905510576','','Marie-Kristine','Tardif','mktardif@vt.edu','','','','','Paid through Fall','','','','','','','','','','','','','p','p','p','','p',NULL),(NULL,NULL,205,'','','Hannah','Thomas','','','','friend- Myles','','','','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,206,'','','Ian','Tillotson','','U','spring 12','','','','','','','p','p','p','','','','','','','','','','','p',NULL),(NULL,NULL,207,'','','Vladimir','Timoshevskiy','timvl@vt.edu','','10 (couple)','','','10','','','','','','','','','','','','p','','','','','',NULL),(NULL,NULL,208,'','','Kelsey','Trimble','','U','','','','','','','','','p','','','','','','','','','','','','',NULL),(NULL,NULL,209,'905428040','','Craig','Tripp','','U','','','','','','x','p','','p','p','','','','','','','','','p','','',NULL),(NULL,NULL,210,'umbel8@vt.edu','','Rachel','Umbel','umbel8@vt.edu','U','','','','','','','','','','','','','','','','','','','','p','',NULL),(NULL,NULL,211,'','','Hannah','Utter','','','','','','','','','','','','','','p','','','','','','','','','',NULL),(NULL,NULL,212,'905602058','','Vanessa','Vanderdys','vvand93@vt.edu','','','','','15 2-2-2012','','','','','','','','','','','','','p','p','p','','',NULL),(NULL,NULL,213,'','','Abby','Vandivier','vabby10@vt.edu','U','','','','','','','','','p','','','','','','','','','','','','',NULL),(NULL,NULL,214,'','','Andrew','Vorel','advorel@vt.edu','U','','Dining Card','','','','','','P','','','','','','','','','','','','','',NULL),(NULL,NULL,215,'905287642','','Guanying','Wang','','','','','','','','','','','','','','','','','','','','p','p','','',NULL),(NULL,NULL,216,'','','Kat','Warwick','kwarwick@vt.edu','','','','','','p','','p','p','','','','','','','','','','','','','',NULL),('2013-09-13 00:00:00',NULL,217,'905503619','','Alison','Webster','','U','spring 2012','','','','p','','','p','p','p','','','','p','p','p','p','','','','p',NULL),(NULL,NULL,218,'','','Nathanael','Welch','','','','','','','','','','','','p','','','p','','p','','','','','','',NULL),(NULL,NULL,219,'','','Chris','Wemple','wchris4@vt.edu','','Spring 2012','','','$25 October','p','p','p','p','p','p','','','','','','','','','','','',NULL),(NULL,NULL,220,'','','Hope','Wentzel','','','','','','','','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,221,'905444599','','Karin','Wiles','','U','Spring 2012','','','','p','p','p','p','p','p','p','p','p','','p','','p','p','','','',NULL),(NULL,NULL,222,'','','John','Williams','','U','','Friends','','','','','','','p','','','','','','','','','','','','',NULL),(NULL,NULL,223,'','','KC','Winters','kc11@vt.edu','u','','','','','','','','','','p','','p','','','','','','','','','',NULL),(NULL,NULL,224,'','','Jeremiah','Witte','','u','','','','','p','p','p','p','','p','','','','','p','','','','','','',NULL),(NULL,NULL,225,'905523666','','Huigun','Xiong','','','','','','','','','','','','','','','','','','','','p','p','','',NULL),(NULL,NULL,226,'','','Bryan','Yanchulis','bryany07@vt.edu','','','','','','p','','','','','','','','','','','','','','','','',NULL),(NULL,NULL,227,'905636779','','Felix','Yanqing','fyq@vt.edu','g','11 fall','searched for it','','15','','','','p','','','','p','','','','','','','','','',NULL),(NULL,NULL,228,'','','Benn','York','','','','','','','','x','p','p','','','','','','','','','p','','','','',NULL),(NULL,NULL,229,'905667279','','Yin','Yuan','','','','','','','','','','','','','','','','','','','','p','p','','',NULL),(NULL,NULL,230,'','','David','Zamore','','U','','','','','','','','','p','','','p','','','','','','','','','',NULL),(NULL,NULL,231,'','','Callie','Zawaski','zawaca@vt.edu','U','','FB','','','','x','','','','','','','','','','','','','p','','',NULL),(NULL,NULL,232,'905542329','','Emily','Zhang','','U','12 spring','','','25','p','','p','p','p','p','','p','p','','p','','','','','','p',NULL),('2012-09-12 00:00:00',NULL,233,'905448813','','Stephanie','Hippert','','','','','','','','','','','','','','','','','','','','p','','','p',NULL),(NULL,NULL,241,'905254785','cespedes','Christian','Cespedes','jasonc2@vt.edu','','','karin wiles','','','','','','','','','','','','','','','','','','','',''),(NULL,NULL,242,'905489787','kwangs7','Kwangseon','Hwang','kwangs7@vt.edu','','','Anthony','','','','','','','','','','','','','','','','','','','',''),(NULL,NULL,243,'000000000','schererj','Jason','Scherer','schererj@vt.edu','','','Website','','','','','','','','','','','','','','','','','','','',''),(NULL,NULL,244,'000000000','schererj','Jason','Scherer','schererj@vt.edu','','','Website','','','','','','','','','','','','','','','','','','','',''),(NULL,NULL,245,'000000000','kewiles1','Karin','Wiles','karinw@vt.edu','','','karin wiles','','','','','','','','','','','','','','','','','','','',''),(NULL,NULL,246,'000000000','kewiles1','Karin','Wiles','karinw@vt.edu','','','test method','','','','','','','','','','','','','','','','','','','',''),(NULL,NULL,248,'905533718','chxthump','Charlotte','Crump','','','','Kim Schatz','','','','','','','','','','','','','','','','','','','',''),(NULL,NULL,249,';90542666','skj2013','Sarah','Johnson','','','','Karin Wiles','','','','','','','','','','','','','','','','','','','',''),(NULL,NULL,250,'905433526','tannerh2','Tanner','Hansinger','','','','Website','','','','','','','','','','','','','','','','','','','',''),(NULL,NULL,251,'905571433','wldegrl4','Grace','Wilde','','','','test method','','','','','','','','','','','','','','','','','','','',''),(NULL,NULL,252,'905570443','eleonorc','Eleonor','Cayford','','','','Jen Gorce','','','','','','','','','','','','','','','','','','','',''),(NULL,NULL,253,'905519952','cprother','Courtney','Prothero','','','','Table Cards (West En','','','','','','','','','','','','','','','','','','','',''),(NULL,NULL,254,'905399849','hdwhite','Harry','White','','','','Friend','','','','','','','','','','','','','','','','','','','',''),(NULL,NULL,255,'905466925','nathan09','Nathan','Friedlander','','','','Friend','','','','','','','','','','','','','','','','','','','',''),(NULL,NULL,256,'703599150','','Samir','MIr-Aliyev','','','','Table Cards (West En','','','','','','','','','','','','','','','','','','','',''),(NULL,NULL,257,'905494213','bkullbac','Brendan','Kullback','','','','Website','','','','','','','','','','','','','','','','','','','',''),(NULL,NULL,258,'905580076','dsarah7','Sarah','Desilva','','','','Friend','','','','','','','','','','','','','','','','','','','',''),(NULL,NULL,259,'905614505','arouffa','Alycia','Rouffa','','','','Friend','','','','','','','','','','','','','','','','','','','',''),(NULL,NULL,260,'905559142','gcynthia','Georgette','Bastien','','','','Friend','','','','','','','','','','','','','','','','','','','',''),(NULL,NULL,261,'905577142','emack434','Elizabeth','Mack','','','','Table Cards (West En','','','','','','','','','','','','','','','','','','','',''),('2012-09-12 00:00:00',NULL,262,'905379812','georgeb5','George','Buhl','','','','Karin Wiles','','','','','','','','','','','','','','','','','','','',''),(NULL,NULL,263,'905476921','kshepard','Kathryn','Shepard','','','','Anthony','','','','','','','','','','','','','','','','','','','',''),(NULL,NULL,264,'905367300','crroa7','Christian','Roa','','','','Friend','','','','','','','','','','','','','','','','','','','',''),(NULL,NULL,265,'905083525','sshaffer','Stephen','Shaffer','','','','Friend','','','','','','','','','','','','','','','','','','','',''),('2012-09-12 00:00:00','2012-09-11 13:50:28',266,'111111111','sosso','Anthony','Sosso','','','','1','','','','','','','','','','','','','','','','','','','',''),('2012-09-12 00:00:00','2012-09-11 13:53:10',267,'222222222','sosso','Anthony','Sosso','','','','25','','','','','','','','','','','','','','','','','','','',''),('2013-03-17 00:00:00','2012-09-11 00:00:00',268,'905678245','benwr','Benjamin','Weinstein-Raun','','','','1','','','','','','','','','','','','','','','','','','','',''),('2012-09-12 00:00:00','2012-09-11 00:00:00',269,'905510576','mktardif','Marie-Kristine','Tardif','','','','24','','','','','','','','','','','','','','','','','','','',''),('2012-09-12 00:00:00','2012-09-11 00:00:00',270,'905714266','wxz001','Zachariah','Ewen','','','','25','','','','','','','','','','','','','','','','','','','',''),('2012-09-12 00:00:00','2012-09-11 00:00:00',271,'905714266','wxz001','Zachariah','Ewen','','','','25','','','','','','','','','','','','','','','','','','','',''),('2012-09-12 00:00:00','2012-09-11 00:00:00',272,'905714266','wxz001','Zachariah','Ewen','','','','25','','','','','','','','','','','','','','','','','','','',''),('2012-09-12 00:00:00','2012-09-11 00:00:00',273,'905714266','wxz001','Zachariah','Ewen','','','','25','','','','','','','','','','','','','','','','','','','',''),('2012-09-12 00:00:00','2012-09-11 00:00:00',274,'905746413','cguh10','Christina','Guh','','','','25','','','','','','','','','','','','','','','','','','','',''),('2012-09-12 00:00:00','2012-09-11 00:00:00',275,'905720043','jfrate3','jordan','Frate','','','','24','','','','','','','','','','','','','','','','','','','',''),('2012-09-12 00:00:00','2012-09-11 00:00:00',276,'905739003','dabbott2','David','Abbott','','','','25','','','','','','','','','','','','','','','','','','','',''),('2012-09-12 00:00:00','2012-09-11 00:00:00',277,'905710757','steph94','Stephen','Buscemi','','','','24','','','','','','','','','','','','','','','','','','','',''),('2012-09-12 00:00:00','2012-09-11 00:00:00',278,'905720018','cle1234','Christina','Eiginger','','','','25','','','','','','','','','','','','','','','','','','','',''),('2012-09-12 00:00:00','2012-09-11 00:00:00',279,'905701312','ellenma','Ellen','Augst','','','','25','','','','','','','','','','','','','','','','','','','',''),('2012-09-12 00:00:00','2012-09-11 00:00:00',280,'905695485','bwitcher','Bennett','Witcher','','','','25','','','','','','','','','','','','','','','','','','','',''),('2012-09-12 00:00:00','2012-09-11 00:00:00',281,'905739327','aaronk1','Aaron','Kunzer','','','','25','','','','','','','','','','','','','','','','','','','',''),('2012-09-12 00:00:00','2012-09-11 00:00:00',282,'905602363','buv1028','Brianne','Varnerin','','','','1','','','','','','','','','','','','','','','','','','','',''),('2012-09-12 00:00:00','2012-09-11 00:00:00',283,'905740784','','Daniella','Damiano','','','','1','','','','','','','','','','','','','','','','','','','',''),('2012-09-12 00:00:00','2012-09-11 00:00:00',284,'905712605','kinds94','Kindell','Schmitt','','','','25','','','','','','','','','','','','','','','','','','','',''),('2012-09-12 00:00:00','2012-09-11 00:00:00',285,'905737805','paulw93','Paul','Wotirng','','','','25','','','','','','','','','','','','','','','','','','','',''),('2012-09-12 00:00:00','2012-09-11 00:00:00',286,'905727137','gabzulu','Gabriella','Jacobsen','','','','1','','','','','','','','','','','','','','','','','','','',''),('2012-09-12 00:00:00','2012-09-11 00:00:00',287,'905698130','haugenmc','Mitchell','Haugen','','','','25','','','','','','','','','','','','','','','','','','','',''),('2012-09-12 00:00:00','2012-09-11 00:00:00',288,'905723364','fmegan94','Megan','Freyman','','','','25','','','','','','','','','','','','','','','','','','','',''),('2012-09-12 00:00:00','2012-09-11 00:00:00',289,'905723566','schen518','Susan','Chen','','','','25','','','','','','','','','','','','','','','','','','','',''),('2012-09-12 00:00:00','2012-09-11 00:00:00',290,'905723620','jenlu','Jennifer','Lu','','','','25','','','','','','','','','','','','','','','','','','','',''),('2012-09-12 00:00:00','2012-09-11 00:00:00',291,'905719490','justn12','Justin','Niles','','','','1','','','','','','','','','','','','','','','','','','','',''),('2012-09-12 00:00:00','2012-09-11 00:00:00',292,'905696670','demetr3','Demetrius','Lunsford','','','','25','','','','','','','','','','','','','','','','','','','',''),('2012-09-12 00:00:00','2012-09-11 00:00:00',293,'905694573','maduq7','Marcus','Duquiatan','','','','25','','','','','','','','','','','','','','','','','','','',''),('2012-09-12 00:00:00','2012-09-11 00:00:00',294,'905576905','legoman','Patrick','Harrington','','','','24','','','','','','','','','','','','','','','','','','','',''),('2012-09-12 00:00:00','2012-09-11 00:00:00',295,'905742831','rblaura5','Laura','Bunn','','','','25','','','','','','','','','','','','','','','','','','','',''),('2013-09-06 00:00:00','2012-09-11 00:00:00',296,'905695378','vivianct','Vivian','Tran','','','','25','','','','','','','','','','','','','','','','','','','',''),('2013-09-06 00:00:00','2012-09-11 00:00:00',297,'905735576','biancave@vt.edu','Bianca','Eckerberg','','','','24','','','','','','','','','','','','','','','','','','','',''),('2013-03-10 00:00:00','2012-09-11 00:00:00',298,'905739728','bkatrina','Katrina','Buccella','','','','24','','','','','','','','','','','','','','','','','','','',''),('2013-09-13 00:00:00','2012-09-18 00:00:00',299,'111111121','sosso','Anthony','Sosso','','','','13','','','','','','','','','','','','','','','','','','','',''),('2013-09-13 00:00:00','2012-09-18 00:00:00',300,'000000009','sosso','Anthony','Sosso','','','','1','','','','','','','','','','','','','','','','','','','',''),('2013-03-17 00:00:00','2012-09-18 00:00:00',301,'905547948','smh2418','Sarah ','Hunsberger','','','','25','','','','','','','','','','','','','','','','','','','',''),('2012-09-19 00:00:00','2012-09-18 00:00:00',302,'905719533','wanalise','Analise','Wallick','','','','24','','','','','','','','','','','','','','','','','','','',''),('2012-09-19 00:00:00','2012-09-18 00:00:00',303,'905464600','dagarwa','Divyanshu','Agarwal','','','','1','','','','','','','','','','','','','','','','','','','',''),('2012-09-19 00:00:00','2012-09-18 00:00:00',304,'905418421','donalm91','Donal','Murphy','','','','1','','','','','','','','','','','','','','','','','','','',''),('2012-09-19 00:00:00','2012-09-18 00:00:00',305,'905761393','mariant','Marian','Turowski','','','','25','','','','','','','','','','','','','','','','','','','',''),('2012-09-19 00:00:00','2012-09-18 00:00:00',306,'905473903','rainfall','Rui','Ma','','','','1','','','','','','','','','','','','','','','','','','','','');
/*!40000 ALTER TABLE `signin_member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `signin_professionalevent`
--

DROP TABLE IF EXISTS `signin_professionalevent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `signin_professionalevent` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `signin_professionalevent`
--

LOCK TABLES `signin_professionalevent` WRITE;
/*!40000 ALTER TABLE `signin_professionalevent` DISABLE KEYS */;
INSERT INTO `signin_professionalevent` VALUES (1,'2012-09-18');
/*!40000 ALTER TABLE `signin_professionalevent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `signin_professionalrecord`
--

DROP TABLE IF EXISTS `signin_professionalrecord`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `signin_professionalrecord` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `professional_event_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `signin_professionalrecord_56e38b98` (`member_id`),
  KEY `signin_professionalrecord_cd83209e` (`professional_event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `signin_professionalrecord`
--

LOCK TABLES `signin_professionalrecord` WRITE;
/*!40000 ALTER TABLE `signin_professionalrecord` DISABLE KEYS */;
INSERT INTO `signin_professionalrecord` VALUES (1,191,1),(2,300,1),(3,301,1),(4,169,1),(5,104,1),(6,302,1),(7,303,1),(8,304,1),(9,305,1),(10,306,1);
/*!40000 ALTER TABLE `signin_professionalrecord` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `signin_record`
--

DROP TABLE IF EXISTS `signin_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `signin_record` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `signin_record_1647d06b` (`event_id`),
  KEY `signin_record_56e38b98` (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `signin_record`
--

LOCK TABLES `signin_record` WRITE;
/*!40000 ALTER TABLE `signin_record` DISABLE KEYS */;
INSERT INTO `signin_record` VALUES (1,191,2),(2,2,2),(3,3,2),(4,5,2),(5,6,2),(6,10,2),(7,13,2),(8,14,2),(9,17,2),(10,21,2),(11,22,2),(12,23,2),(13,31,2),(14,37,2),(15,39,2),(16,41,2),(17,44,2),(18,46,2),(19,48,2),(20,50,2),(21,51,2),(22,52,2),(23,53,2),(24,54,2),(25,55,2),(26,56,2),(27,58,2),(28,60,2),(29,64,2),(30,69,2),(31,70,2),(32,72,2),(33,73,2),(34,74,2),(35,76,2),(36,77,2),(37,83,2),(38,84,2),(39,85,2),(41,90,2),(42,96,2),(43,98,2),(44,102,2),(45,107,2),(46,115,2),(47,116,2),(48,122,2),(49,125,2),(50,128,2),(51,130,2),(52,131,2),(53,139,2),(54,144,2),(55,145,2),(56,147,2),(57,149,2),(58,151,2),(59,152,2),(60,155,2),(61,157,2),(62,158,2),(63,160,2),(64,162,2),(65,163,2),(66,166,2),(67,169,2),(68,171,2),(69,175,2),(70,178,2),(71,179,2),(72,183,2),(73,186,2),(74,187,2),(75,188,2),(76,189,2),(77,191,2),(78,192,2),(79,198,2),(80,200,2),(81,201,2),(82,203,2),(83,216,2),(84,217,2),(85,219,2),(86,221,2),(87,224,2),(88,226,2),(89,232,2),(90,234,2),(91,3,3),(92,6,3),(93,10,3),(94,18,3),(95,20,3),(96,22,3),(97,26,3),(98,43,3),(99,47,3),(100,60,3),(101,61,3),(102,64,3),(103,69,3),(104,76,3),(106,108,3),(107,113,3),(108,121,3),(109,122,3),(110,124,3),(111,141,3),(112,146,3),(113,152,3),(114,162,3),(115,177,3),(116,182,3),(117,186,3),(118,190,3),(119,191,3),(120,195,3),(121,209,3),(122,219,3),(123,221,3),(124,224,3),(125,228,3),(126,231,3),(127,234,3),(128,2,4),(129,3,4),(130,17,4),(131,18,4),(132,21,4),(133,22,4),(134,31,4),(135,37,4),(136,41,4),(137,50,4),(138,52,4),(139,60,4),(140,61,4),(141,64,4),(142,69,4),(143,72,4),(144,73,4),(145,76,4),(146,77,4),(148,90,4),(149,102,4),(150,121,4),(151,122,4),(152,128,4),(153,139,4),(154,146,4),(155,152,4),(156,157,4),(157,177,4),(158,179,4),(159,182,4),(160,183,4),(161,186,4),(162,189,4),(163,191,4),(164,209,4),(165,216,4),(166,219,4),(167,221,4),(168,224,4),(169,228,4),(170,232,4),(171,234,4),(172,2,5),(173,6,5),(174,20,5),(175,22,5),(176,30,5),(177,31,5),(178,45,5),(179,46,5),(180,52,5),(181,59,5),(182,64,5),(183,69,5),(184,72,5),(185,75,5),(186,76,5),(187,77,5),(188,79,5),(189,80,5),(191,90,5),(192,92,5),(193,97,5),(194,101,5),(195,105,5),(196,110,5),(197,122,5),(198,124,5),(199,128,5),(200,130,5),(201,138,5),(202,141,5),(203,146,5),(204,152,5),(205,153,5),(206,157,5),(207,160,5),(208,169,5),(209,178,5),(210,179,5),(211,182,5),(212,186,5),(213,191,5),(214,199,5),(215,203,5),(216,206,5),(217,214,5),(218,216,5),(219,217,5),(220,219,5),(221,221,5),(222,224,5),(223,227,5),(224,228,5),(225,232,5),(226,234,5),(227,1,6),(228,2,6),(229,4,6),(230,6,6),(231,8,6),(232,11,6),(233,20,6),(234,22,6),(235,31,6),(236,42,6),(237,47,6),(238,48,6),(239,52,6),(240,64,6),(241,69,6),(242,76,6),(244,87,6),(245,91,6),(246,109,6),(247,112,6),(248,122,6),(249,124,6),(250,138,6),(251,143,6),(252,146,6),(253,150,6),(254,157,6),(255,178,6),(256,179,6),(257,182,6),(258,186,6),(259,191,6),(260,206,6),(261,208,6),(262,209,6),(263,213,6),(264,217,6),(265,219,6),(266,221,6),(267,222,6),(268,230,6),(269,232,6),(270,234,6),(271,2,7),(272,3,7),(273,6,7),(274,22,7),(275,31,7),(276,36,7),(277,37,7),(278,45,7),(279,47,7),(280,50,7),(281,52,7),(282,54,7),(283,59,7),(284,64,7),(285,66,7),(286,69,7),(287,72,7),(288,75,7),(289,76,7),(290,77,7),(292,90,7),(293,117,7),(294,122,7),(295,124,7),(296,136,7),(297,139,7),(298,146,7),(299,157,7),(300,159,7),(301,168,7),(302,169,7),(303,178,7),(304,179,7),(305,180,7),(306,181,7),(307,186,7),(308,191,7),(309,193,7),(310,194,7),(311,196,7),(312,197,7),(313,199,7),(314,206,7),(315,209,7),(316,217,7),(317,218,7),(318,219,7),(319,221,7),(320,223,7),(321,224,7),(322,232,7),(323,234,7),(324,186,8),(325,191,8),(326,221,8),(327,234,8),(328,3,9),(329,6,9),(330,15,9),(331,16,9),(332,22,9),(333,25,9),(334,49,9),(335,60,9),(336,65,9),(337,67,9),(338,69,9),(339,72,9),(340,73,9),(341,75,9),(342,76,9),(343,77,9),(345,90,9),(346,100,9),(347,119,9),(348,120,9),(349,124,9),(350,126,9),(351,127,9),(352,140,9),(353,141,9),(354,157,9),(355,160,9),(356,169,9),(357,177,9),(358,178,9),(359,179,9),(360,182,9),(361,184,9),(362,186,9),(363,191,9),(364,196,9),(365,211,9),(366,221,9),(367,223,9),(368,227,9),(369,230,9),(370,232,9),(371,234,9),(372,2,10),(373,3,10),(374,6,10),(375,9,10),(376,20,10),(377,22,10),(378,31,10),(379,32,10),(380,36,10),(381,37,10),(382,52,10),(383,59,10),(384,65,10),(385,67,10),(386,69,10),(387,71,10),(388,72,10),(389,73,10),(390,76,10),(391,77,10),(393,90,10),(394,103,10),(395,104,10),(396,110,10),(397,113,10),(398,122,10),(399,124,10),(400,129,10),(401,137,10),(402,140,10),(403,141,10),(404,142,10),(405,146,10),(406,148,10),(407,168,10),(408,169,10),(409,177,10),(410,178,10),(411,179,10),(412,182,10),(413,184,10),(414,186,10),(415,191,10),(416,218,10),(417,221,10),(418,232,10),(419,234,10),(420,6,11),(421,22,11),(422,25,11),(423,37,11),(424,50,11),(425,67,11),(426,69,11),(427,72,11),(428,73,11),(429,76,11),(430,77,11),(432,95,11),(433,103,11),(434,122,11),(435,124,11),(436,129,11),(437,146,11),(438,149,11),(439,157,11),(440,168,11),(441,169,11),(442,178,11),(443,179,11),(444,182,11),(445,186,11),(446,191,11),(447,196,11),(448,217,11),(449,234,11),(450,22,12),(451,25,12),(452,37,12),(453,50,12),(454,63,12),(455,69,12),(456,72,12),(457,75,12),(458,76,12),(459,77,12),(461,122,12),(462,124,12),(463,157,12),(464,160,12),(465,168,12),(466,169,12),(467,179,12),(468,182,12),(469,191,12),(470,194,12),(471,196,12),(472,217,12),(473,218,12),(474,221,12),(475,224,12),(476,232,12),(477,234,12),(478,2,13),(479,3,13),(480,6,13),(481,22,13),(482,50,13),(483,69,13),(484,72,13),(485,75,13),(486,90,13),(487,103,13),(488,122,13),(489,124,13),(490,157,13),(491,168,13),(492,169,13),(493,178,13),(494,179,13),(495,182,13),(496,191,13),(497,194,13),(498,207,13),(499,217,13),(500,234,13),(501,2,14),(502,3,14),(503,20,14),(504,22,14),(505,33,14),(506,34,14),(507,38,14),(508,50,14),(509,65,14),(510,69,14),(511,72,14),(512,73,14),(513,75,14),(514,77,14),(515,88,14),(516,104,14),(517,111,14),(518,122,14),(519,134,14),(520,135,14),(521,141,14),(522,146,14),(523,149,14),(524,164,14),(525,167,14),(526,168,14),(527,169,14),(528,179,14),(529,182,14),(530,186,14),(531,191,14),(532,193,14),(533,204,14),(534,212,14),(535,217,14),(536,221,14),(537,228,14),(538,234,14),(539,2,15),(540,3,15),(541,7,15),(542,10,15),(543,20,15),(544,22,15),(545,35,15),(546,47,15),(547,50,15),(548,65,15),(549,69,15),(550,73,15),(551,74,15),(552,75,15),(553,77,15),(554,93,15),(555,99,15),(556,104,15),(557,106,15),(558,110,15),(559,120,15),(560,122,15),(561,130,15),(562,133,15),(563,135,15),(564,161,15),(565,164,15),(566,167,15),(567,168,15),(568,169,15),(569,170,15),(570,172,15),(571,177,15),(572,179,15),(573,186,15),(574,190,15),(575,191,15),(576,204,15),(577,212,15),(578,215,15),(579,221,15),(580,225,15),(581,229,15),(582,233,15),(583,234,15),(584,2,16),(585,20,16),(586,29,16),(587,38,16),(588,50,16),(589,57,16),(590,73,16),(591,76,16),(592,77,16),(593,90,16),(594,104,16),(595,118,16),(596,122,16),(597,123,16),(598,130,16),(599,132,16),(600,149,16),(601,164,16),(602,167,16),(603,168,16),(604,169,16),(605,172,16),(606,173,16),(607,177,16),(608,180,16),(609,186,16),(610,191,16),(611,204,16),(612,209,16),(613,212,16),(614,215,16),(615,225,16),(616,229,16),(617,231,16),(618,234,16),(619,3,17),(620,26,17),(621,28,17),(622,43,17),(623,73,17),(624,81,17),(625,82,17),(626,88,17),(627,122,17),(628,135,17),(629,141,17),(630,164,17),(631,165,17),(632,169,17),(633,179,17),(634,182,17),(635,186,17),(636,191,17),(637,196,17),(638,210,17),(639,234,17),(640,2,18),(641,3,18),(642,22,18),(643,73,18),(644,75,18),(645,76,18),(646,77,18),(647,90,18),(648,104,18),(649,124,18),(650,141,18),(651,164,18),(652,168,18),(653,169,18),(654,173,18),(655,177,18),(656,179,18),(657,182,18),(658,191,18),(659,204,18),(660,206,18),(661,217,18),(662,232,18),(663,233,18),(664,234,18),(665,191,19),(666,234,19),(668,236,19),(669,234,20),(670,191,20),(671,237,20),(672,238,20),(673,239,20),(674,22,20),(675,3,20),(676,240,20),(677,76,20),(678,82,20),(679,81,20),(680,197,20),(681,104,20),(682,69,20),(683,232,20),(684,179,20),(685,75,20),(686,186,20),(687,77,20),(688,182,20),(689,37,20),(690,164,20),(691,20,20),(692,217,20),(693,194,20),(694,72,20),(695,73,20),(696,177,20),(697,168,20),(698,241,20),(699,141,20),(700,90,20),(701,169,20),(702,242,20),(703,204,20),(704,148,20),(705,221,20),(706,22,22),(707,191,22),(708,186,22),(709,69,22),(710,221,22),(711,177,23),(712,167,23),(713,221,23),(714,22,23),(715,77,23),(716,248,23),(717,172,23),(718,249,23),(719,250,23),(720,251,23),(721,73,23),(722,72,23),(723,179,23),(724,232,23),(725,252,23),(726,253,23),(727,20,23),(728,254,23),(729,76,23),(730,255,23),(731,164,23),(732,104,23),(733,69,23),(734,168,23),(735,2,23),(736,204,23),(737,256,23),(738,81,23),(739,82,23),(740,37,23),(741,169,23),(742,3,23),(743,257,23),(744,90,23),(745,258,23),(746,259,23),(747,260,23),(748,261,23),(749,141,23),(750,122,25),(751,179,25),(752,262,25),(753,263,25),(754,227,25),(755,182,25),(756,2,25),(757,264,25),(758,265,25),(759,76,25),(760,225,25),(761,215,25),(762,169,25),(763,217,25),(764,22,25),(765,191,25),(766,20,25),(767,252,25),(768,77,25),(769,69,25),(770,240,26),(771,266,27),(772,267,27),(773,191,27),(774,268,27),(775,269,27),(776,270,27),(777,271,27),(778,272,27),(779,273,27),(780,77,27),(781,20,27),(782,90,27),(783,274,27),(784,275,27),(785,276,27),(786,277,27),(787,278,27),(788,279,27),(789,280,27),(790,281,27),(791,282,27),(792,179,27),(793,283,27),(794,186,27),(795,69,27),(796,284,27),(797,285,27),(798,286,27),(799,287,27),(800,288,27),(801,289,27),(802,290,27),(803,291,27),(804,292,27),(805,293,27),(806,294,27),(807,295,27),(808,217,27),(809,296,27),(810,297,27),(811,298,27),(812,191,28),(813,300,28),(814,268,28),(815,217,28),(816,301,28),(817,169,28),(818,104,28),(819,302,28),(820,179,28),(821,77,28),(822,303,28),(823,20,28),(824,304,28),(825,168,28),(826,305,28),(827,306,28);
/*!40000 ALTER TABLE `signin_record` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-10-01 14:48:56
