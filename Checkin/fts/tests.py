"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import LiveServerTestCase, TestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from signin.models import Member, Event
import datetime
import selenium.webdriver.support.ui as ui
from signin.views import checkinMember

class SigninTest(LiveServerTestCase):
    fixtures = ['fts/fixtures/advertisingmethod.json', 'fts/fixtures/event.json', 'fts/fixtures/user_attended_event']
    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def _make_event_for_today(self):
        event = Event()
        event.date = datetime.datetime.today()
        event.save()
    
    def _make_member(self):
        member = Member()
        member.idnum = "878787878"
        member.save()
        return member
    
    def test_new_member(self):
        #we need an event for today!
        self._make_event_for_today()
        
        self.browser.get(self.live_server_url)
        # She sees the familiar 'attendance' heading
        body = self.browser.find_element_by_tag_name('body')
        self.assertIn('Ballroom Dance @ Virginia Tech Attendance', body.text)

        # She types in her ID and clicks the submit button
        id_field = self.browser.find_element_by_xpath('//*[@id="id_idnum"]')
        id_field.send_keys('878787878')
        self.browser.find_element_by_xpath('/html/body/center/div/form/input').click()
        

        # ID accepted, but she's a new member.  Ask for her info!
        self.assertEquals(self.browser.current_url, self.live_server_url + '/signin/')
        
        # she enters her PID, and waits for hokiestalker to complete by hitting tab
        pid_field = self.browser.find_element_by_name('pid')
        pid_field.send_keys('sosso')
        pid_field.send_keys(Keys.TAB)
        
        #We wait for hokiestalker to complete its lookup
        WebDriverWait(self.browser, timeout=10).until(lambda d: d.find_element_by_name('major').get_attribute("value") == "Computer Engineering")
        
        #HokieStalker has completed, click submit
        self.browser.find_element_by_xpath('/html/body/div/form/center/input[7]').click()
        
        #We're taken to the 'success' page
        WebDriverWait(self.browser, timeout=10).until(lambda d: d.find_element_by_tag_name('h2').text == 'Check-in Successful!')
        #satisfied, we click the 'home' link
        self.browser.find_element_by_link_text('Home').click()
        self.assertEquals(self.browser.current_url, self.live_server_url + '/')
        #we're home, make sure none of the fields are filled and then exit
        self.assertEquals(self.browser.find_element_by_xpath('//*[@id="id_idnum"]').text, '')

    def test_already_signed_in(self):
        self._make_event_for_today()
        person = self._make_member()
        checkinMember(person)
        
        #Gertrude can't remember if she signed in for the day.
        self.browser.get(self.live_server_url)
        # She sees the familiar 'attendance' heading
        body = self.browser.find_element_by_tag_name('body')
        self.assertIn('Ballroom Dance @ Virginia Tech Attendance', body.text)

        # She types in her ID and clicks the submit button
        id_field = self.browser.find_element_by_xpath('//*[@id="id_idnum"]')
        id_field.send_keys('878787878')
        self.browser.find_element_by_xpath('/html/body/center/div/form/input').click()
        
        # The system processes her ID and should find she's already signed in.
        WebDriverWait(self.browser, timeout=10).until(lambda d: d.current_url == self.live_server_url + '/signin/')
        WebDriverWait(self.browser, timeout=10).until(lambda d: d.find_element_by_tag_name('h2').text == 'You have already signed in today!')
        #Feeling silly for having signed in, she goes into the event!

class foo():
    def bar(self):
        body = self.browser.find_element_by_tag_name('body')
        self.assertIn('Site administration', body.text)

        # She now sees a couple of hyperlink that says "Polls"
        polls_links = self.browser.find_elements_by_link_text('Polls')
        self.assertEquals(len(polls_links), 2)
    
        # The second one looks more exciting, so she clicks it
        polls_links[1].click()
    
        # She is taken to the polls listing page, which shows she has
        # no polls yet
        body = self.browser.find_element_by_tag_name('body')
        self.assertIn('0 polls', body.text)
    
        # She sees a link to 'add' a new poll, so she clicks it
        new_poll_link = self.browser.find_element_by_link_text('Add poll')
        new_poll_link.click()
        
        # She sees some input fields for "Question" and "Date published"
        body = self.browser.find_element_by_tag_name('body')
        self.assertIn('Question:', body.text)
        self.assertIn('Date published:', body.text)
        
        # She types in an interesting question for the Poll
        question_field = self.browser.find_element_by_name('question')
        question_field.send_keys("How awesome is Test-Driven Development?")
    
        # She sets the date and time of publication - it'll be a new year's
        # poll!
        date_field = self.browser.find_element_by_name('pub_date_0')
        date_field.send_keys('01/01/12')
        time_field = self.browser.find_element_by_name('pub_date_1')
        time_field.send_keys('00:00')
        
        # She sees she can enter choices for the Poll.  She adds three
        choice_1 = self.browser.find_element_by_name('choice_set-0-choice')
        choice_1.send_keys('Very awesome')
        choice_2 = self.browser.find_element_by_name('choice_set-1-choice')
        choice_2.send_keys('Quite awesome')
        choice_3 = self.browser.find_element_by_name('choice_set-2-choice')
        choice_3.send_keys('Moderately awesome')
        
        

        # Gertrude clicks the save button
        save_button = self.browser.find_element_by_css_selector("input[value='Save']")
        save_button.click()

        # She is returned to the "Polls" listing, where she can see her
        # new poll, listed as a clickable link
        new_poll_links = self.browser.find_elements_by_link_text(
                "How awesome is Test-Driven Development?"
        )
        self.assertEquals(len(new_poll_links), 1)
