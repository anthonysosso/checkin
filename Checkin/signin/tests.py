from django.test.testcases import TestCase
from django.utils import timezone
from signin.models import Member

class MemberModelTest(TestCase):
    def test_creating_a_new_poll_and_saving_it_to_the_database(self):
        # start by creating a new Poll object with its "question" set
        member = Member()
        member.idnum = '878787878'
        member.first_event_date = timezone.now()

        # check we can save it to the database
        member.save()

        # now check we can find it in the database again
        all_members_in_database = Member.objects.all()
        self.assertEquals(len(all_members_in_database), 1)
        only_member_in_database = all_members_in_database[0]
        self.assertEquals(only_member_in_database, member)

        # and check that it's saved its two attributes: question and pub_date
        self.assertEquals(only_member_in_database.idnum, '878787878')
        self.assertEquals(only_member_in_database.first_event_date, member.first_event_date)
        
    def test_verbose_name_for_first_event_date(self):
        for field in Member._meta.fields:
                if field.name == 'first_event_date':
                    self.assertEquals(field.verbose_name, 'First event attended')
                    
    def test_member_objects_are_named_after_their_pid(self):
        m = Member()
        m.pid = 'riccardo'
        self.assertEquals(unicode(m), 'riccardo')
        
#    def test_poll_can_tell_you_its_total_number_of_votes(self):
#        p = Poll(question='where',pub_date=timezone.now())
#        p.save()
#        c1 = Choice(poll=p,choice='here',votes=0)
#        c1.save()
#        c2 = Choice(poll=p,choice='there',votes=0)
#        c2.save()
#
#        self.assertEquals(p.total_votes(), 0)
#
#        c1.votes = 1000
#        c1.save()
#        c2.votes = 22
#        c2.save()
#        self.assertEquals(p.total_votes(), 1022)        

