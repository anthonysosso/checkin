from django.db import models
from django.utils import timezone
import datetime

class Event(models.Model):
	date = models.DateField()	
	id = models.AutoField(primary_key=True, db_column='id')

	def __unicode__(self):
		return self.date.__str__()

class ProfessionalEvent(models.Model):
	date = models.DateField()	

	def __unicode__(self):
		return self.date.__str__()

class Advertisingmethod(models.Model):
	name = models.CharField(max_length = 30)	
	def __unicode__(self):
		return self.name


class Member(models.Model):
	idnum = models.CharField(max_length = 20, default='')
	pid = models.CharField(max_length = 20, default='')
	firstname = models.CharField(max_length = 20, default='')
	lastname = models.CharField(max_length = 20, default='')
	email = models.CharField(max_length = 20, default='')
	first_event_date = models.DateTimeField(verbose_name='First event attended', default=datetime.date.today())
	membership_expiration_date = models.DateTimeField(verbose_name='Membership expiration date', default=datetime.date.today())
	studenttype = models.CharField(max_length = 20, default='')
	membershipexpiration = models.CharField(max_length = 20, default='') 
	advertisingmethod = models.CharField(max_length = 200, default='')#models.ForeignKey(Advertisingmethod, default='')
	referername = models.CharField(max_length = 20, default='')
	paymentinfo = models.CharField(max_length = 20, default='')
	event1_22_11 = models.CharField(max_length = 20, default='')
	event9_8_11 = models.CharField(max_length = 20, default='')
	event9_10_11 = models.CharField(max_length = 20, default='')
	event9_15_11 = models.CharField(max_length = 20, default='')
	event9_29_11 = models.CharField(max_length = 20, default='')
	event10_6_11 = models.CharField(max_length = 20, default='')
	event10_13_11 = models.CharField(max_length = 20, default='')
	eventsocial10_15= models.CharField(max_length = 20, default='')
	event10_20_11 = models.CharField(max_length = 20, default='')
	event10_27_11 = models.CharField(max_length = 20, default='')
	event11_3_11 = models.CharField(max_length = 20, default='')
	event11_10_11 = models.CharField(max_length = 20, default='')
	event12_1_11 = models.CharField(max_length = 20, default='')
	event1_19_2012 = models.CharField(max_length = 20, default='')
	event1_26_2012 = models.CharField(max_length = 20, default='')
	event2_2_2012 = models.CharField(max_length = 20, default='')
	eventSocial2_4_2012 = models.CharField(max_length = 20, default='')
	event2_9_2012 = models.CharField(max_length = 20, default='')
	def __unicode__(self):
		return self.pid
	def statsID(self):
		if self.idnum != "":
			idstatus = "(has id)"
		else:
			idstatus = "(no id)"
		return self.lastname + ", " + self.firstname + " " + idstatus
	
	def process_membership(self, membership_type):
		if membership_type is None:
			self.membership_expiration_date = datetime.date.today() + datetime.timedelta(days=1)
		elif membership_type == "6 months":
			self.membership_expiration_date = datetime.date.today() + datetime.timedelta(days=6*30)
		elif membership_type == "12 months":
			self.membership_expiration_date = datetime.date.today() + datetime.timedelta(days=12*30)
		else:
			self.membership_expiration_date = datetime.date.today() + datetime.timedelta(days=1)
		self.save()


class Record(models.Model):
	member = models.ForeignKey(Member)
	event = models.ForeignKey(Event)
	def __unicode__(self):
		return self.member.firstname + " " + self.member.lastname + " " + self.event.date.__str__()
	
class ProfessionalRecord(models.Model):
	member = models.ForeignKey(Member)
	professional_event = models.ForeignKey(ProfessionalEvent)
	def __unicode__(self):
		return self.member.firstname + " " + self.member.lastname + " " + self.professional_event.date.__str__()

class MembershipExpiredException(Exception):
	def __init__(self, *args):
		# *args is used to get a list of the parameters passed in
		self.args = [a for a in args]
